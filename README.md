# AngularBlog

This is my personal blog made with Angular version 11.1.1 

I use Firebase for the datebase and hosting.

This is a SPA with admin part , protected by login and password.

Its possible to create, update and delete posts from admin account.


## See live

[My happy blog](https://angular-blog-717db.web.app/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


